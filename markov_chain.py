#!/usr/bin/env python3


import sys
import os
import time
import io
import re
import glob
import json
import requests
import feedparser
import random
import scipy.sparse
from lxml import etree

from pdb import set_trace as BP


SRC_URLS = [
	"https://kohlchan.net/" + p + "/index.rss" for p in [
		"b", "pol", "alt", "keller", "bus", "nvip", "tu", "km"
	]
]

POST_TEXTS = dict()
POST_TEXTS_FNAME = "post_texts"
MARKOV_CHAIN = dict()


WORD_MASK = re.compile( "|".join((
	r"(?:\w+:\s)?https?:\S*",
	"[()#»«™\n\r\"'„“]",
	"->",
	r"[:]\w+[:]", # failed icons
	r"\w*?(\w)\1{3,}\S*", # REEEEEEE
	r"\s*[Tt]\.\s*\S+$", # t. bla
	r"^\S+$", # single word
	r"\s+\w\s+|^\w\s+|\s+\w$", # single char
	r"\[-\]", # [-]
	# r"\w+-?/",
	r"\w*@\w+(\.\w+)?", # email, twitter
	"[^-A-Za-z0-9äÄöÖüÜß .,_!?()<>^*+:;/–%€$]",
	"^[-:,\s]+|[-:,\s]+$", # clean up tails
)))

WORD_REPLACE = [ [re.compile(p), r] for p, r in (
	[ r"\s*[–…]+\s*", ", " ],
	[ r"\s{2,}", " " ],
	[ r"([-.,?!]){2,}", r"\1" ],
	[ r"\s+([.,?!])([^\W_]|$)", r"\1\2" ],
	[ r"(\s-)(\w+)", r"-\2" ],
	[ r"(\w+)[.\n\r]+(\w+)", r"\1. \2" ],
	[ r"([A-Za-zÄäÖöÜüß]+),+(\w+)", r"\1, \2" ],
	[ r"([:;xX])+([/O0D3]){2,}", r" \1\2" ],
	[ r"([.*O;^])(_{2,})[.*O;^]", r"\1_\1" ],
	[ "(HA){5,}", "HAHAHAHA" ],
	[ "(HA\s+){2,}", "HAHAHAHA " ],
	[ r"(^|\s)[_#~(\$]+([\w ]+?)[_#~)]+($|\s)", r"\1\2\3" ],
	[ r"(\w+)-?/-?\w+", r"\1" ], # word asd-/word
	[ r"(^|\s)\$(\w+)(\W|$)", r"\1\2\3" ],
	[ r"(^|\s)([/<>.])+(\s|$)", r"\2" ], # \s <CHAR>*2.. \s
	#[ r"([^;30/.!?])$", r"\1." ],

	[ r"\s*(?<=[^.!?])$", r"." ], # set ending '.' this has to be the last regex
)]
HAS_WORD_REPLACE = re.compile("|".join(( r.pattern for r, _ in WORD_REPLACE )))

IRREGULAR_SENTENCE_END = re.compile( "|".join([
	"[^\w]*(" + p + r")\.+$" for p in [
		"[.*O;^]_+[.*O;^]", "[;:xX][/O0D3^]+", r"\*\w+\*", r"/\w+",
	]]))

WORD_DISCARD = re.compile( "|".join( ( r"\s+" + w + r"\s+" for w in (
	"for", "the", "of", "get", "it", "but", "are", "were", "by", "me",
	"that", "from", "can", "to", "and", "have", "has", "had", "a",
	"you",
))))


HEADERS = [
	{
		'User-Agent':
			'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) '
			'AppleWebKit/537.36 (KHTML, like Gecko) '
			'Chrome/39.0.2171.95 Safari/537.36'
	},
	{
		'User-Agent':
			'Mozilla/5.0 (X11; Linux x86_64; rv:78.0) '
			'Gecko/20100101 Firefox/78.0'
	},
	{
		'User-Agent':
			'Mozilla/5.0 (Linux; U; Android 2.2) '
			'AppleWebKit/533.1 (KHTML, like Gecko) '
			'Version/4.0 Mobile Safari/533.1'
	},
	{
		'User-Agent':
			'Mozilla/5.0 (Windows NT 10.0; Win64; x64) '
			'AppleWebKit/537.36 (KHTML, like Gecko) '
			'Chrome/74.0.3729.169 Safari/537.36'
	}
]
for h in HEADERS:
	h.update({
		'Host': 'kohlchan.net',
		'Accept': 'text/html',
	})


def gen_timestamp():
	return time.strftime('%Y-%m-%d_%H-%M-%S')


def read_timestamp_from(text):
	try:
		timestamp = re.search(r"^[-_\d]+\d", os.path.basename(text))[0]
	except Exception as e:
		e.args = e.args + (f"Failed to extract timestamp from '{text}'",)
		raise
	return timestamp


def load_post_texts_from_file(warn=False, normalize=False):
	global POST_TEXTS

	files = glob.glob("./*.json")

	for f in files:
		print("Reading '%s' ..." % f)
		timestamp = read_timestamp_from(f)

		with open(f) as fc:
			if not normalize:
				POST_TEXTS[timestamp] = set(json.load(fc))
			else:
				for post in json.load(fc):
					add_post_text_to_dict(timestamp, post, warn, normalize)


def add_post_text_to_dict(timestamp, post, warn=False, normalize=False):
	global POST_TEXTS

	if not post:
		return

	if timestamp not in POST_TEXTS:
		POST_TEXTS.update({ timestamp : set() })

	if normalize:
		post = normalize_text(post, warn)

	if post not in ( p for vals in POST_TEXTS.values() for p in vals ):
		POST_TEXTS[timestamp].add(post)

	elif warn:
		print("\n".join([
			"Duplicate post detected in",
			str([ k for k, v in POST_TEXTS.items() if post in v ]),
			"POST:", post
		]))


def store_post_texts_to_file(f=POST_TEXTS_FNAME):
	for timestamp, posts in POST_TEXTS.items():
		file_name = f"{timestamp}_{f}.json"

		if os.path.isfile(file_name):
			continue

		try:
			json.dump(list(posts), open(file_name, 'w'), indent=1)
		except Exception as e:
			e.args = e.args + (f"Failed to store file '{file_name}'",)
			raise


def crawl_source(board_url, timestamp=gen_timestamp()):
	global POST_TEXTS

	rss_feed = feedparser.parse(board_url)

	for thread_url in ( e.link for e in rss_feed.entries ):
		print(f"Requesting: {thread_url}")

		thread_tree = etree.parse(
			io.StringIO(requests.get(
				thread_url, headers=random.choice(HEADERS)).text),
			etree.HTMLParser()
		).getroot()

		for post in thread_tree.findall("**//div[@class='divMessage']"):
			add_post_text_to_dict(timestamp, post.text)

		time.sleep(2)


def init_markov_chain(n_max_word_seq=3):
	global MARKOV_CHAIN
	MARKOV_CHAIN = dict()

	if n_max_word_seq <= 0:
		raise ValueError("Maximum word sequence length n_max_word_seq must be > 0")

	posts = lambda : ( post.split() for v in POST_TEXTS.values() for post in v )
	words = lambda : ( word for post in posts() for word in post )
	k_word_seqs = lambda k : (
		seq for seq in ( post[i:i+k] for post in posts() for i in range(len(post)) )
		if len(seq) == k
	)
	word_idx_dict = { word : i for i, word in enumerate(list(set(words()))) }
	n_unique_words = len(word_idx_dict)

	for seq_length in range(n_max_word_seq + 1, 0, -1):
		word_seqs = list(k_word_seqs(seq_length))
		BP()
		n_unique_sequences = len(list(set( word_seqs )))
		BP()

		MARKOV_CHAIN.update({
			seq_length : scipy.sparse.dok_matrix((n_unique_words, n_unique_sequences))
		})
		BP()

		for i, w_seq in enumerate(word_seqs):
			BP()
			pass


		print(n_unique_words)
		print(n_unique_sequences)
		print(len(list(k_word_seqs(3))))
		print(list(k_word_seqs(3)))
		print(len(word_idx_dict))
		BP()


def normalize_text(post_text, warn=False):
	ret = str(post_text) if post_text else ""

	if WORD_DISCARD.search(ret, re.IGNORECASE):
		ret = ""

	while WORD_MASK.search(ret) or HAS_WORD_REPLACE.search(ret):
		ret = " ".join(( w for w in WORD_MASK.split(ret) if w ))

		for rgx, subst in ([r, s] for r, s in WORD_REPLACE if r.search(ret)):
			ret = rgx.sub(subst, ret)

	if IRREGULAR_SENTENCE_END.search(ret):
		re_match_group_idx = next(filter(all,
			enumerate(IRREGULAR_SENTENCE_END.findall(ret)[0], 1)
		))[0]

		ret = IRREGULAR_SENTENCE_END.sub(f" \\{re_match_group_idx}", ret)

	if warn and post_text and ret != post_text:
		print_norm_diff(post_text, ret)

	return ret


def print_norm_diff(in_text, out_text=""):
	if in_text == out_text:
		return

	print( "\n".join([
		"--- Text normalized ---",
		"IN:", str(in_text),
		"OUT:", normalize_text(in_text),
		"MASK:", str([
			p for p in WORD_MASK.pattern.split("|") if re.search(p, in_text)
		]),
		"SUB:", str([
			[ r.pattern, s ] for r, s in WORD_REPLACE if r.search(in_text)
		]),
		"DISCARD", str([
			p for p in WORD_DISCARD.pattern.split("|") if re.search(p, in_text)
		]),
		"SENTENCE END", str([
			p for p in IRREGULAR_SENTENCE_END.pattern.split("|")
			if re.search(p, in_text)
		])
	]))

	if "bp" in sys.argv:
		BP()


def generate_text(f=POST_TEXTS_FNAME):
	init_markov_chain()

	# TODO remove
	for k in list(MARKOV_CHAIN):
		if len(k) <= 1:
			MARKOV_CHAIN.pop(k)

	words = [ get_rnd_capital_word() ]

	while words[-1][-1] != ".":
		words.append(get_rnd_word(words[-1]))

	return " ".join(words)


def get_rnd_word(seed):
	return ""


def get_rnd_capital_word():
	return ""


if __name__ == "__main__":
	#TODO handle cli text file param -> argparse
	debug = "debug" in sys.argv

	if "crawl" in sys.argv:
		POST_TEXTS = dict()

		print("Loading stored post texts (not normalized).")
		load_post_texts_from_file(warn=debug, normalize=False)

		timestamp = gen_timestamp()

		for url in SRC_URLS:
			crawl_source(url, timestamp)

		print("Storing new post texts. '%s'" % timestamp)
		store_post_texts_to_file()


	if "gen" in sys.argv:
		POST_TEXTS = dict()

		print("Loading stored post texts (normalized).")
		load_post_texts_from_file(warn=debug, normalize=True)

		while True:
			print(generate_text())
			BP()

	if not any(o in sys.argv for o in ["crawl", "gen"]):
		print("USAGE: ./markov_chain.py [crawl, gen] [debug, bp]")

