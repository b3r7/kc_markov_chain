#! /usr/bin/env python3

import markov_chain as mc
import unittest
from pdb import set_trace as BP


class NormalizationTest(unittest.TestCase):
	def apply_tests(self, test_data):
		for raw_text, result in test_data:
			try:
				self.assertEqual(mc.normalize_text(raw_text), result)
			except Exception as e:
				print(mc.normalize_text(raw_text, True))
				print(e)
				BP()
				raise


	def identity_tests(self):
		self.apply_tests([
			[ "word: word", "word: word." ],
			[ "foo bar?", "foo bar?" ],
			[ "foo bar!", "foo bar!" ],
			[ "word :^", "word :^" ],
			[ "word O_O", "word O_O" ],
			[ "word ^_^", "word ^_^" ],
			[ "word asd>foo", "word asd>foo." ],
			[ "asd 1.23 foo", "asd 1.23 foo." ],
		])


	def smileys(self):
		self.apply_tests([
			[ "word :33 word", "word :3 word." ],
			[ "word. :DDD", "word :D" ],
			[ "word word *___*", "word word *_*" ],
			[ "word .__.", "word ._." ],
			[ "word O___O", "word O_O" ],
			[ "word ^__^", "word ^_^" ],
			# [ "word :D :D", "word :D" ],
		])


	def single_words(self):
		self.apply_tests([
			[ "word", "" ],
			[ ":3", "" ],
			[ ";____;", "" ],
		])


	def irregular_words(self):
		self.apply_tests([
			[ "HAHAHAHAHA word", "HAHAHAHA word." ],
			[ "HA HA HA word", "HAHAHAHA word."],
			[ "~word word", "word word." ],
			[ "_word_ word", "word word." ],
			[ "word _word_ word", "word word word." ],
			[ "word _word word_", "word word word." ],
			[ "word (((word)))", "word word." ],
			[ "word (word word)", "word word word." ],
			[ "word – word – word", "word, word, word." ],
			[ "30NG word 30%", "30NG word 30%." ],
			[ "word 30NG word", "word 30NG word." ],
			[ "word asd-/word", "word asd." ],
			[ "word asd-/word-/word", "word asd." ],
			[ "asd -foo bar-baz", "asd-foo bar-baz." ],
			[ "word asd -foo-/bar/-baz", "word asd-foo." ],
			[ "foo /b/ bar", "foo /b/ bar." ],
			[ "asd /int/", "asd /int/." ],
			[ "/bus/ foo", "/bus/ foo." ],
			[ "word: http://bla", "" ],
			[ "asd foo word: http://bla", "asd foo." ],
			[ "asd foo word: http://bla bar", "asd foo bar." ],
		])


	def sentence_edings(self):
		self.apply_tests([
			[ "word word t. Muhh", "word word." ],
			[ "t. word/word", "" ],
			[ "word T. word", "" ],
			[ "word, word t.asd", "word, word." ],
			[ "word. /word", "word /word" ],
			[ "word! :D", "word :D" ],
			[ "asd :D.", "asd :D" ],
			[ "asd foo:", "asd foo." ],
			[ "word word…", "word word." ],
			[ "asd foo!!!", "asd foo!" ],
		])


	def illegal_characters(self):
		self.apply_tests([
			[ "word word…", "word word."],
			[ "'''word word'''", "word word." ],
			[ "بر word", "" ],
			[ "بر word word", "word word." ],
			[ "$asd foo $bar?", "asd foo bar?" ],
			[ "foo® bar™", "foo bar." ],
			[ "word // word", "" ],
			[ "word / word asd", "word asd." ],
			[ "„asd foo“ word", "asd foo word." ],
			[ "asd foo: „word", "asd foo: word." ],
			[ "asd foo.“ word", "asd foo. word." ],
			[ "»word asd« foo", "word asd foo." ],
			[ "word asd >>>> foo", "word asd>foo." ],
			[ "asd << foo word", "asd<foo word." ],
			[ ":: ::", "" ],
			[ "word word .", "word word." ],
			[ "word word .word", "word word. word." ],
			[ "asd @foo word", "asd word." ],
		])



	def test(self):
		self.identity_tests()
		self.smileys()
		self.single_words()
		self.irregular_words()
		self.sentence_edings()
		self.illegal_characters()


if __name__ == "__main__":
	unittest.main()
